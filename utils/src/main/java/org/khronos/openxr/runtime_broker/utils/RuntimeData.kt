// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
package org.khronos.openxr.runtime_broker.utils

import org.khronos.openxr.runtime_broker.utils.BrokerContract.BrokerType
import android.content.pm.ResolveInfo
import android.content.ComponentName
import android.os.Bundle
import java.util.HashMap
import java.lang.ClassCastException
import android.content.pm.ApplicationInfo
import android.content.pm.ServiceInfo
import android.util.Log
import java.util.regex.Pattern

/**
 * Data corresponding to a single OpenXR runtime.
 *
 *
 * Populated from service-associated metadata keys, this data is the rough equivalent of the data
 * supported by the JSON manifest format used by the OpenXR loader on other platforms.
 */
class RuntimeData {
    /**
     * The ServiceInfo used to locate the runtime, if available.
     */
    val serviceInfo: ServiceInfo?

    /**
     * The package name associated with the runtime
     */
    val packageName: String

    /**
     * The native library directory.
     *
     * @todo This might carry the wrong ABI-specific suffix, how to fix?
     */
    val nativeLibraryDir: String

    /**
     * The filename of the .so file containing the runtime entry points.
     *
     *
     * Indicated by the "org.khronos.openxr.OpenXRRuntime.SoFilename" meta-data string value on the
     * "org.khronos.openxr.OpenXRRuntimeService" service manifest.
     */
    val soFilename: String

    /**
     * The major version of OpenXR implemented.
     *
     *
     * Indicated by the "org.khronos.openxr.OpenXRRuntime.MajorVersion" meta-data string value on
     * the "org.khronos.openxr.OpenXRRuntimeService" service manifest.
     */
    val majorVersion: Long

    /**
     * A map of function names to symbol names, akin to the object of the same name in the JSON
     * manifest format.
     *
     *
     * May be empty, as the corresponding JSON object is optional and may be empty.
     *
     *
     * Indicated by metadata string values  on the "org.khronos.openxr.OpenXRRuntimeService" service
     * manifest with keys starting with "org.khronos.openxr.OpenXRRuntime.Functions." The remaining
     * part of the meta-data key is taken to be the key in this map (the specified function name),
     * while the value is the symbol name that should be used.
     */
    val functions: Map<String, String>

    /**
     * The broker type returning this runtime.
     *
     *
     * Usually can be ignored. Used in the implementation of the installable runtime broker to proxy
     * the results of the system runtime broker.
     */
    val brokerType: BrokerType

    /**
     * Constructor
     *
     * @param packageName      the package name associated with the OpenXR runtime
     * @param nativeLibraryDir Full path to the directory containing the .so file
     * @param soFilename       the filename of the .so file containing the runtime entry points
     * @param majorVersion     the major version of OpenXR implemented
     * @param functions        Function name map
     * @param brokerType       What type of broker returns this. If null, the default (installable) is assumed.
     */
    constructor(
        packageName: String,
        nativeLibraryDir: String,
        soFilename: String,
        majorVersion: Int,
        functions: Map<String, String>,
        brokerType: BrokerType?
    ) {
        serviceInfo = null
        this.packageName = packageName
        this.nativeLibraryDir = nativeLibraryDir
        this.soFilename = soFilename
        this.majorVersion = majorVersion.toLong()
        this.functions = functions
        if (brokerType == null) {
            this.brokerType = BrokerType.RuntimeBroker
        } else {
            this.brokerType = brokerType
        }
    }

    /**
     * Constructor
     *
     * @param serviceInfo      the ServiceInfo associated with the OpenXR runtime
     * @param nativeLibraryDir Full path to the directory containing the .so file
     * @param soFilename       the filename of the .so file containing the runtime entry points
     * @param majorVersion     the major version of OpenXR implemented
     * @param functions        Function name map
     * @param brokerType       What type of broker returns this. If null, the default (installable) is assumed.
     */
    constructor(
        serviceInfo: ServiceInfo,
        nativeLibraryDir: String,
        soFilename: String,
        majorVersion: Int,
        functions: Map<String, String>,
        brokerType: BrokerType?
    ) {
        this.serviceInfo = serviceInfo
        packageName = serviceInfo.packageName
        this.nativeLibraryDir = nativeLibraryDir
        this.soFilename = soFilename
        this.majorVersion = majorVersion.toLong()
        this.functions = functions
        if (brokerType == null) {
            this.brokerType = BrokerType.RuntimeBroker
        } else {
            this.brokerType = brokerType
        }
    }

    companion object {
        private const val TAG = "RuntimeData"

        private const val soMetadataName = "org.khronos.openxr.OpenXRRuntime.SoFilename"
        private const val versionMetadataName = "org.khronos.openxr.OpenXRRuntime.MajorVersion"
        private const val functionsMetadataPrefix = "org.khronos.openxr.OpenXRRuntime.Functions."
        private val functionNameRegex = Pattern.compile("xr[A-Z]([a-z0-9]*)([0-9A-Z]([a-z0-9]*))*")

        private fun parseFunctionMetadataKey(key: String): String? {
            if (!key.startsWith(functionsMetadataPrefix)) {
                return null
            }
            val funcName = key.substring(functionsMetadataPrefix.length)
            // for safety, check format of key
            return if (functionNameRegex.matcher(key).matches()) {
                funcName
            } else null
        }

        /**
         * Get the ComponentName for a Service
         *
         * @param resolveInfo a ResolveInfo from PackageManager.queryIntentServices() with an
         * org.khronos.openxr.OpenXRRuntimeService Intent and
         * PackageManager.GET_META_DATA
         * @return the ComponentName, or null if resolveInfo.serviceInfo is null
         */
        private fun getServiceComponent(resolveInfo: ResolveInfo): ComponentName? {
            val serviceInfo = resolveInfo.serviceInfo ?: return null
            return if (serviceInfo.packageName == null || serviceInfo.name == null) {
                null
            } else ComponentName(serviceInfo.packageName, serviceInfo.name)
        }

        /**
         * Get the functions map, if any, for an OpenXR runtime.
         *
         * @param bundle the metadata bundle from the service resolve info.
         * @return a map of specified function name to exposed symbol, if any were specified in the
         * associated manifest meta-data. May be empty.
         */
        private fun getFunctionsMap(bundle: Bundle): Map<String, String> {
            val functions = HashMap<String, String>()
            for (key in bundle.keySet()) {
                val function = parseFunctionMetadataKey(key) ?: continue
                val symbol: String? = try {
                    bundle.getString(key)
                } catch (e: ClassCastException) {
                    continue
                }
                if (symbol != null && symbol.isNotEmpty()) {
                    functions.putIfAbsent(function, symbol)
                }
            }
            return functions
        }

        /**
         * Get the runtime data for an OpenXR runtime.
         *
         * @param resolveInfo a ResolveInfo from PackageManager.queryIntentServices() with an
         * org.khronos.openxr.OpenXRRuntimeService Intent and
         * PackageManager.GET_META_DATA
         * @return the data object, or null if there was an issue, including if the service was not an
         * OpenXR runtime.
         */
        fun fromIntentResolveInfo(resolveInfo: ResolveInfo, abi: String): RuntimeData? {
            val serviceInfo = resolveInfo.serviceInfo ?: return null
            val applicationInfo = serviceInfo.applicationInfo ?: return null
            val bundle = serviceInfo.metaData
            if (bundle == null) {
                Log.w(TAG, "No service metadata for " + serviceInfo.name)
                return null
            }
            val info = resolveInfo.serviceInfo ?: return null
            if (!bundle.containsKey(soMetadataName)) {
                return null
            }
            if (!bundle.containsKey(versionMetadataName)) {
                return null
            }
            val functions = getFunctionsMap(bundle)
            return RuntimeData(
                info,
                getAbiNativeLibraryDir(applicationInfo, abi),
                bundle.getString(soMetadataName)!!,
                bundle.getInt(versionMetadataName),
                functions,
                null
            )
        }

        private fun getAbiNativeLibraryDir(applicationInfo: ApplicationInfo, abi: String): String {
            // STOPSHIP: 11/11/2020 this isn't doing anything right now, but should.
            // unfortunately the useful members of ApplicationInfo are marked UnsupportedAppUsage and Hide
            return applicationInfo.nativeLibraryDir
        }
    }
}