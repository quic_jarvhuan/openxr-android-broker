// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
package org.khronos.openxr.runtime_broker

import org.khronos.openxr.broker_lib.AbstractRuntimeBroker
import org.khronos.openxr.broker_lib.BrokerUriParser
import org.khronos.openxr.broker_lib.RuntimeBrokerUriParser
import org.khronos.openxr.runtime_broker.utils.RuntimeChooser

/// This class implements a runtime broker by drawing together the runtime chooser
/// and the URI parser for the installable broker.
class InstallableRuntimeBroker : AbstractRuntimeBroker() {
    override val runtimeChooser: RuntimeChooser = InstallableRuntimeChooser()
    override val parser: BrokerUriParser = RuntimeBrokerUriParser()
}